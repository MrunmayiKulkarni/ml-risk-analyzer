# ML Risk Analyzer

Techniques: Machine Learning Models (Classification)
Overview and Problem Statement:
Credit risk is associated with the possibility of a client failing to meet contractual obligations, such as mortgages, credit card debts, and other types of loans. A payment default occurs when you fail to pay the minimum amount due on the credit card for a few consecutive months. Usually, the default notice is sent by the card issuer after 6 consecutive missed payments. Machine Learning models have been helping these companies to improve the accuracy of their credit risk analysis, providing a scientific method to identify potential debtors in advance.

Can you reliably predict who is likely to default? If so, the bank may be able to prevent the loss by providing the customer with alternative options (such as forbearance or debt consolidation, etc.). In this project, the participants are expected to (i) perform exploratory data analysis and derive meaningful insights and (ii) perform pre-processing and features correlation and (iii) develop a machine learning model to predict whether a customer is a credit card defaulter or not.

Dataset Description:
The dataset ‘Default of credit card clients’ contains information on default payments, demographic factors, credit data, history of payment, and bill statements of credit card clients in Taiwan from April 2005 to September 2005.





 


